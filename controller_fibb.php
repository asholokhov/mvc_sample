<?php

require_once './view_fibb.php';
require_once './model_fibb.php';

abstract class ControllerBase {
    protected $model;
    protected $view;
    
    protected $rawData;
    
    public function __construct($data = null) {
	if (isset($data))
	    $this->rawData = $data;
	else
	    $this->rawData = null;
	
	$this->model = null;
	$this->view  = null;
    }
    
    public function setControllerData($data) {
	if (isset($data))
	    $this->rawData = $data;
    }
    
    abstract public function defeaultAction();
    
}

class ControllerFibbonaci extends ControllerBase {
    
    public function __construct($data = null) {
	parent::__construct($data);
	
	$this->model = new ModelFibbonaci($this->rawData);
	$this->view  = new ViewFibbonaci();
    }


    public function defeaultAction() {
	echo $this->view->defeaultView();
    }
    
    public function getFibbonaciTable() {
	$this->model->setRawData($this->rawData);
	$this->model->calculate();
	echo $this->view->tableView($this->model->getCalculatedData());
    }
    
}

?>
