<?php
    ini_set('display_errors', 1);
    require_once './controller_fibb.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
	<style>
	    table td {
		border: 1px black solid;
	    }
	    
	    table tr:nth-child(odd) {
		background-color: bisque;

	    }
	    
	    table tr:nth-child(1) {
		background-color: gold;
		font-weight: bold;
	    }
	</style>
    </head>
    <body>
	<?php
	    $controller = new ControllerFibbonaci();
	    	
	    if (!isset($_GET['fibb_count']) || !is_int((int)$_GET['fibb_count'])) {
		$controller->defeaultAction();
	    } else {
		$_GET['fibb_count'] = (int)$_GET['fibb_count'];
		
		$controller->setControllerData($_GET['fibb_count']);
		$controller->getFibbonaciTable();
	    }	
	?>
    </body>
</html>
